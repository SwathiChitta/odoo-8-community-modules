{
    "name": 'Product barcode generator',
    "version": '1.0',
    "description": """
    *This module will add a function which leads to an automatic generation of EAN13 for products

    You will have to define the company default value (6 firsts number of EAN13) then the 6 next number the sequence.
    The 13rd is the key of the EAN13, this will be automatically computed.
    """,
    "author": 'ehAPI',
    "website": 'http://www.ehapi.com',
    "depends": ['base', 'product',],
    "demo": [],
    "data": [
        "data/ean_sequence.xml",
        "res_company_view.xml",
        "product_view.xml",
        "sequence_view.xml",
    ],
    "installable": True,
    "active": False,
    "category": "Stock Management",
}
