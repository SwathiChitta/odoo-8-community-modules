from openerp.osv import fields, orm


class res_company(orm.Model):

    _inherit = 'res.company'

    _columns = {
        'ean_sequence_id': fields.many2one('ir.sequence', 'Ean Sequence'),
    }
