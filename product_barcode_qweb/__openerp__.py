{
    'name': 'Product Barcode QWeb',
    'version': '1.0.1',
    'category': 'Barcode',
    'sequence': 6,
    'summary': 'Barcode label for products',
    'description': """
        Print Barcode label for products
    """,
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',

    'images': [
    ],
    'depends': ['product','stock'],
    'data': [
        'wizard/product_barcode_print.xml',
        'views/report_paperformat.xml',
        'product_barcode_qweb_report.xml',
        'views/report_product_barcode.xml',
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'application': True,
    #'qweb': ['static/src/xml/pos.xml'],
    'auto_install': False,
}

