#from barcode.writer import ImageWriter
#from barcode import generate
from Code128 import Code128
import base64
from StringIO import StringIO

import time
import math
from openerp.osv import osv
from openerp.report import report_sxw

import os
from openerp.tools import config
my_data_directory = os.path.join(config['data_dir'], "odoo-8.0", "product_barcode_qweb")
# logging.warning(my_data_directory+' ####################data directory')

if not os.path.exists(my_data_directory):
    os.makedirs(my_data_directory)


class product_barcode_print(report_sxw.rml_parse):

    def _getLabelRows(self, form):
        product_obj = self.pool.get('product.product')
        data = []
        result = {}
        product_ids = form['product_ids']
        from_row = form['row'] or 1
        from_column = form['column'] or 1
        start_print = False
        if not product_ids:
            return {}
        
        products_data = product_obj.read(self.cr, self.uid, product_ids, ['name','default_code','list_price','ean13','categ_id','company_id'])
        count_data = len(products_data)
        count = 0
        while count < count_data:
           product = products_data[count]
           product_count = False
           product_row = 0
           label_count = 0
           
           while label_count < form['qty']:
                label_row=[]
                for row in [1]:
                    if (product_row == (from_row - 1)) and (row == from_column):
                        start_print = True
                    if start_print:
                        label_data = {
                            'name': product['name'],
                            'default_code': product['ean13'] or product['default_code'],
                            'price': product['list_price'],
                            'category':product['categ_id'][1] if product['categ_id'] else '',
                            'company':product['company_id'][1] if product['company_id'] else ''
                        }
                        label_row.append(label_data)
                        product_count = True
                        label_count += 1
                    else:
                        label_data = {
                            'name': '',
                            'default_code': '',
                            'price': '',
                            'category':'',
                            'company':''
                        }
                        label_row.append(label_data)
                data.append(label_row)
                product_row += 1
           if product_count:
                count += 1
            
        if data:
            return data
        else:
            return {}

    def _generateBarcode(self, barcode_string):  #, height, width):
        fp = StringIO()
        #generate('CODE39', barcode_string, writer=ImageWriter(), add_checksum=False, output=fp)
        #barcode_data = base64.b64encode(fp.getvalue())
        #return '<img style="width: 25mm;height: 7mm;" src="data:image/png;base64,%s" />'%(barcode_data)
        #return barcode_data
        Code128().getImage(barcode_string, path=my_data_directory).save(fp,"PNG")
        barcode_data = base64.b64encode(fp.getvalue())
        return barcode_data

    def __init__(self, cr, uid, name, context):
        super(product_barcode_print, self).__init__(cr, uid, name, context=context)
        self.total = 0.0
        self.qty = 0.0
        self.total_invoiced = 0.0
        self.discount = 0.0
        self.total_discount = 0.0
        self.localcontext.update({
            'time': time,
            'getLabelRows':self._getLabelRows,
            'generateBarcode':self._generateBarcode,
        })


class report_product_barcode_print(osv.AbstractModel):
    _name = 'report.product_barcode_qweb.report_product_barcode'
    _inherit = 'report.abstract_report'
    _template = 'product_barcode_qweb.report_product_barcode'
    _wrapped_report_class = product_barcode_print

