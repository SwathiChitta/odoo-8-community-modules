import time
from openerp.osv import osv, fields

import logging
_logger = logging.getLogger(__name__)

class product_barcode_print(osv.osv_memory):
    _name = 'product.barcode.print'
    _description = 'Product Barcode Print'

    _columns = {
        #'date_start': fields.date('Date Start', required=True),
        #'date_end': fields.date('Date End', required=True),
        'qty': fields.integer('Number of labels', help='How many labels to print'),
        'product_ids': fields.many2many('product.product', 'product_barcode_print_product_product_rel', 'product_id', 'wizard_id', 'Products to print labels'),
        'column':fields.integer('From Column'),
        'row':fields.integer('From Row'),
    }
    #_defaults = {
    #    'date_start': lambda *a: time.strftime('%Y-%m-%d'),
    #    'date_end': lambda *a: time.strftime('%Y-%m-%d'),
    #}

    def print_report(self, cr, uid, ids, context=None):
        """
         To get the parameters and print the report
         @param self: The object pointer.
         @param cr: A database cursor
         @param uid: ID of the user currently logged in
         @param context: A standard dictionary
         @return : retrun report
        """
        if context is None:
            context = {}
        datas = {'ids': context.get('active_ids', [])}
        res = self.read(cr, uid, ids, ['qty', 'product_ids','row','column'], context=context)
        res = res and res[0] or {}
        datas['form'] = res
        if res.get('id',False):
            datas['ids']=[res['id']]
        _logger.warning(datas)

        return self.pool['report'].get_action(cr, uid, [], 'product_barcode_qweb.report_product_barcode', data=datas, context=context)

