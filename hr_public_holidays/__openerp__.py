{
    'name': 'HR Public Holidays',
    'version': '8.0.1.0.0',
    # 'license': 'AGPL-3',
    'category': 'Human Resources',
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',
    'summary': "Manage Public Holidays",
    'depends': [
        'hr',
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_public_holidays_view.xml',
    ],
    'installable': True,
}
