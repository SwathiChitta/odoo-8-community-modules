{
    "name": "Dashboard Tile",
    "summary": "Add Tiles to Dashboard",
    "version": "8.0.4.0.0",
    "depends": [
        'web', 'board', 'mail', 'web_widget_color',
    ],
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',
    "category": "web",
    # 'license': 'AGPL-3',
   
    'data': [
        'views/tile.xml',
        'views/templates.xml',
        'security/ir.model.access.csv',
        'security/rules.xml',
    ],
    'demo': [
        'demo/res_groups.yml',
        'demo/tile_tile.yml',
    ],
    'qweb': [
        'static/src/xml/custom_xml.xml',
    ],
}
