from openerp.osv import osv,fields

class Custom_users(osv.osv):

	_inherit = 'res.users'

	def create(self,cr,uid,vals,context=None):
		context.update({'no_reset_password':True})
		return super(Custom_users,self).create(cr,uid,vals,context=context)
