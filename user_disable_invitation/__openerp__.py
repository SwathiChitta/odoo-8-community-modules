{
	'name': "Disable User Invitation",
	'version': '1.0.0',
	'author': 'ehAPI',
	'website': 'http://www.ehapi.com',
	'category': 'tools',
	'depends': ['base'],
	'data': [
		'hide_email_notification.xml',
		],
	'installable': True,
}
