
{
    "name": 'web_widget_datepicker_options',
    "version": "8.0.1.0.0",
    "depends": [
        'base',
        'web',
    ],
    "data": [
        'view/qweb.xml',
    ],
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',

    "installable": True,
    "active": False,
}
