
openerp.web_widget_datepicker_options = function (instance) {

    "use strict";

    instance.web.form.FieldDatetime.include({
        initialize_content: function() {
            this._super();
            var self = this;
            if (this.datewidget) {
                if (typeof this.options.datepicker === 'object') {
                    $.map(this.options.datepicker, function(value, key) {
                        self.datewidget.picker('option', key, value);
                    });
                }
            }
        }
    });

    instance.web.form.FieldDate.include({
        initialize_content: function() {
            this._super();
            var self = this;
            if (this.datewidget) {
                if (typeof this.options.datepicker === 'object') {
                    $.map(this.options.datepicker, function(value, key) {
                        self.datewidget.picker('option', key, value);
                    });
                }
            }
        }
    });
};

