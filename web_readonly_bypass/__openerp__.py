{
    'name': 'Read Only ByPass',
    'version': '8.0.1.0.1',
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',
    'category': 'Technical Settings',
    'depends': [
        'web',
    ],
    'summary': 'Allow to save onchange modifications to readonly fields',
    'data': [
        'views/readonly_bypass.xml',
    ],
    'installable': True,
    'auto_install': False,
}
