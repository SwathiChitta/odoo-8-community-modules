{
    "name": "Remove odoo.com bindings",
    "version": "1.1",
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',
    # "license": "AGPL-3",
    "complexity": "normal",
    "description": """
This module deactivates all bindings to openerp.com that
come with the standard code:

* update notifier code is deactivated and the function is overwritten
* apps and updates menu items in settings are hidden inside Tools\Parameters
* help and account menu items in user menu are removed
* prevent lookup of OPW for current database uuid and resulting
  'unsupported' warning
    """,
    "category": "",
    "depends": [
        'base',
        'mail',
    ],
    "data": [
        "views/disable_openerp_online.xml",
        'views/ir_ui_menu.xml',
        'data/ir_cron.xml',
    ],
    "qweb": [
        'static/src/xml/base.xml',
    ],
    "installable": True,
}
