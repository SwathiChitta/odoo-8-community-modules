{
    'name' : 'Delete "Sent by..." footer in email',
    'version' : '1.0.0',
    'author': 'ehAPI',
    'website': 'http://www.ehapi.com',
    'category' : 'Debranding',
    'depends' : ['mail'],
    'data':[
        ],
    'installable': True
}
